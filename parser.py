import logging
from collections import OrderedDict

logger = logging.getLogger(__name__)


class Parser:

    def __init__(self, data_handler):
        self.data_handler = data_handler

    def parse(self, clue):
        words = clue.split(" ")
        num_words = len(words)

        positions = []
        position = 0
        for i in range(num_words):
            positions.append(position)
            position += len(words[i]) + 1

        sequences = OrderedDict()
        for seq_length in range(1, num_words+1):
            for start_idx in range(0, num_words-seq_length+1):
                position = positions[start_idx]
                this_seq = " ".join(words[start_idx:start_idx+seq_length])
                if this_seq in sequences:
                    sequences[this_seq].append(position)
                else:
                    sequences[this_seq] = [position]

        found_data = OrderedDict()
        for seq, position in sequences.items():
            if seq in self.data_handler:
                found_data[seq] = self.data_handler[seq]
                found_data[seq]["positions"] = position

        return found_data
