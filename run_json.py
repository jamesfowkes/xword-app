""" run_json.py

Usage:
    run_json.py <filename>

"""

import docopt
from xword_app import create_app

from parser import Parser
from data_handler import JSONDataHandler

args = docopt.docopt(__doc__)

data_handler = JSONDataHandler(args["<filename>"])
parser = Parser(data_handler)

app = create_app("", data_handler, parser)

app.run(debug=True)
