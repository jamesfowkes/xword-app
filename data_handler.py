import json
import logging

from pydantic.v1.utils import deep_update

logger = logging.getLogger(__name__)


class JSONDataHandler:

    def __init__(self, json_filepath):
        self.json_filepath = str(json_filepath)
        try:
            with open(self.json_filepath, "r") as f:
                self.__data = json.load(f)
        except FileNotFoundError:
            logger.error(f"File {json_filepath} not found")
            self.__data = {}
            self.__save()

    def __getitem__(self, key):
        return self.__data[key]

    def __setitem__(self, key, val):
        self.__data[key] = val
        self.__save()

    def update(self, new_data):
        self.__data = deep_update(self.__data, new_data)
        self.__save()

    def __save(self):
        with open(self.json_filepath, "w") as f:
            json.dump(self.__data, f)

    def __contains__(self, item):
        return item in self.__data
