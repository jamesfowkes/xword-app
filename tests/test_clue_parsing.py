from parser import Parser

from tests.data import PARSER_DATA_ACTUAL_WORDS, PARSER_DATA_SIMPLE


class TestClueParsing:
    def test_parsing_empty_clue_returns_empty_dict(self):
        # Setup
        parser = Parser(PARSER_DATA_ACTUAL_WORDS)
        # Test
        assert parser.parse("") == {}

    def test_parsing_single_word_clue_not_in_data_returns_empty_dict(self):
        # Setup
        parser = Parser(PARSER_DATA_ACTUAL_WORDS)
        # Test
        assert parser.parse("abc") == {}

    def test_parsing_single_word_clue_in_data_returns_correct_data(self):
        # Setup
        parser = Parser(PARSER_DATA_ACTUAL_WORDS)
        # Test
        assert parser.parse("python") == {
            "python": {"meanings": ["snake", "language"], "positions": [0]},
        }

    def test_parsing_repeated_word_clue_in_data_returns_correct_data(self):
        # Setup
        parser = Parser(PARSER_DATA_ACTUAL_WORDS)
        # Test
        assert parser.parse("python python") == {
            "python": {"meanings": ["snake", "language"], "positions": [0, 7]},
        }

    def test_parsing_multiple_words_in_data_returns_correct_data(self):
        # Setup
        parser = Parser(PARSER_DATA_ACTUAL_WORDS)
        # Test
        assert parser.parse("python test") == {
            "python": {"meanings": ["snake", "language"], "positions": [0]},
            "test": {"meanings": ["challenge", "examine"], "positions": [7]}
        }

    def test_parsing_multiple_overlapping_words_in_data_returns_correct_data(self):
        # Setup
        parser = Parser(PARSER_DATA_ACTUAL_WORDS)
        # Test
        assert parser.parse("test case") == {
            "test": {
                "meanings": ["challenge", "examine"], "positions": [0]
            },
            "case": {
                "indicators": {"ends": "both", "container": True},
                "meanings": ["container,vessel,storage", "instance,occurrence"],
                "positions": [5]
            },
            "test case": {
                "meanings": ["a case to set example/precedent in law", "a particular test of a software component"],
                "positions": [0]
            }
        }

    def test_parsing_lots_of_overlapping_words_in_data_returns_correct_data(self):
        # Setup
        parser = Parser(PARSER_DATA_SIMPLE)

        # Test
        assert parser.parse("a b c d") == {
            "a": {"1": 1, "positions": [0]}, "b": {"2": 2, "positions": [2]}, "c": {"3": 3, "positions": [4]}, "d": {"4": 4, "positions": [6]},
            "a b": {"12": 12, "positions": [0]}, "b c": {"23": 23, "positions": [2]},
            "b c d": {"234": 234, "positions": [2]},
            "a b c d": {"1234": 1234, "positions": [0]}
        }

    def test_parsing_lots_of_repeated_words_in_data_returns_correct_data(self):
        # Setup
        parser = Parser(PARSER_DATA_SIMPLE)

        # Test
        assert parser.parse("a a b a c d") == {
            "a": {"1": 1, "positions": [0, 2, 6]}, "b": {"2": 2, "positions": [4]}, "c": {"3": 3, "positions": [8]}, "d": {"4": 4, "positions": [10]},
            "a b": {"12": 12, "positions": [2]}
        }
