class TestAPIParse:
    def test_parsing_empty_clue_returns_empty_dict(self, client):
        # Setup

        # Action
        response = client.post("/api/parse", data="")

        # Test
        assert response.json == {}

    def test_parsing_single_word_clue_not_in_data_returns_empty_dict(self, client):
        # Setup

        # Action
        response = client.post("/api/parse", data="abc")

        # Test
        assert response.json == {}

    def test_parsing_single_word_clue_in_data_returns_correct_data(self, client):
        # Setup

        # Action
        response = client.post("/api/parse", data="python")

        # Test
        assert response.json == {
            "python": {"meanings": ["snake", "language"], "positions": [0]},
        }

    def test_parsing_repeated_word_clue_in_data_returns_correct_data(self, client):
        # Setup

        # Action
        response = client.post("/api/parse", data="python python")

        # Test
        assert response.json == {
            "python": {"meanings": ["snake", "language"], "positions": [0, 7]},
        }

    def test_parsing_multiple_words_in_data_returns_correct_data(self, client):
        # Setup

        # Action
        response = client.post("/api/parse", data="python test")

        # Test
        assert response.json == {
            "python": {"meanings": ["snake", "language"], "positions": [0]},
            "test": {"meanings": ["challenge", "examine"], "positions": [7]}
        }

    def test_parsing_multiple_overlapping_words_in_data_returns_correct_data(self, client):
        # Setup

        # Action
        response = client.post("/api/parse", data="test case")

        # Test
        assert response.json == {
            "test": {
                "meanings": ["challenge", "examine"], "positions": [0]
            },
            "case": {
                "indicators": {"ends": "both", "container": True},
                "meanings": ["container,vessel,storage", "instance,occurrence"],
                "positions": [5]
            },
            "test case": {
                "meanings": ["a case to set example/precedent in law", "a particular test of a software component"],
                "positions": [0]
            }
        }
