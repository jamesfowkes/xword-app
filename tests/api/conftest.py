import pytest
import logging

from xword_app import create_app

from data_handler import JSONDataHandler
from parser import Parser


@pytest.fixture()
def app():
    data_handler = JSONDataHandler("tests/actual_words.json")
    parser = Parser(data_handler)
    app = create_app(None, data_handler, parser)
    app.config.update({
        "TESTING": True,
    })

    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()
