import pathlib
import json

THIS_DIR = pathlib.Path(__file__).parent

with (THIS_DIR / "actual_words.json").open() as f:
    PARSER_DATA_ACTUAL_WORDS = json.load(f)

with (THIS_DIR / "simple.json").open() as f:
    PARSER_DATA_SIMPLE = json.load(f)
