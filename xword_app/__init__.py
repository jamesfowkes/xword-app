from flask import Flask
from data_handler import JSONDataHandler
from parser import Parser


def create_app(config_filename: str, data_handler: JSONDataHandler, parser: Parser):
    app = Flask(__name__)
    if config_filename:
        app.config.from_pyfile(config_filename)

    from xword_app.api_endpoints import api
    from xword_app.html_endpoints import html

    app.register_blueprint(api)
    app.register_blueprint(html)

    app.data_handler = data_handler
    app.parser = parser

    return app
