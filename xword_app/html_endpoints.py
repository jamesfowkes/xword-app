import flask
from flask import current_app, Blueprint

html = Blueprint('html', __name__, url_prefix='')


@html.route("basic/parse/<string:to_parse>", methods=["GET"])
def basic_parse(to_parse: str) -> flask.typing.ResponseReturnValue:
    parsed = current_app.parser.parse(to_parse)

    output = []

    for token, data in parsed.items():
        meanings = ", ".join(data["meanings"])

        if "indicators" in data:
            indicators = ""

            if "ends" in data["indicators"]:
                indicators += f"initials: {data['indicators']['ends']}"

            token_string = f"{token}: {meanings} ({indicators})"
        else:
            token_string = f"{token}: {meanings}"

        output.append(token_string)

    return "<br/>".join(output)


@html.route("basic/add", methods=["GET"])
def basic_save() -> flask.typing.ResponseReturnValue:
    token = flask.request.args.get('t')
    meanings = flask.request.args.get('m', "").split(",")
    current_app.data_handler.update({token: {"meanings": meanings}})

    return flask.redirect(flask.url_for('basic_parse', to_parse=token))
