import flask
import logging

from flask import request, current_app, Blueprint

api = Blueprint('api', __name__, url_prefix='/api')

logger = logging.getLogger(__name__)


@api.route("parse", methods=["POST"])
def parse() -> flask.typing.ResponseReturnValue:
    data = request.get_data(as_text=True)
    parse_result = flask.jsonify(current_app.parser.parse(data))
    logger.debug(f"{request.url} got data '{data}', parsed as '{parse_result.json}'")
    return parse_result
